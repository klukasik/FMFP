-- This file is loaded automatically if you start GHCi in Code Expert.
-- Feel free to add your own definitions!

-- Computes the greatest common divisor (gcd) of two numbers.
myGcd :: Int -> Int -> Int
myGcd x y
  | x == y    = x
  | y > x     = myGcd y x
  | otherwise = myGcd (x - y) y

-- a)  

a = myGcd 139629 83496

t_a = do
  putStrLn ("a) " ++ (show a))

-- if one of the arguments is negative, the code will run infinitely
-- if one of the arguments is 0, the code will run infinitely 

-- b)

-- abs and absGcd
absGcd :: Int -> Int -> Int
absGcd x y = myGcd (abs x) (abs y)

b = absGcd (-139629) 83496

t_b = do
  putStrLn ("b) " ++ (show b))

-- no, if we input 0, then endlessly

-- c)

c = gcd 139629 83496

t_c = do
  putStrLn ("c) " ++ (show c))

-- d)

myGcdDouble :: Double -> Double -> Double
myGcdDouble x y 
  | x == y    = x
  | y > x     = myGcdDouble y x
  | otherwise = myGcdDouble (x - y) y

d = myGcdDouble 3.6 7.2

t_d = do
  putStrLn("d) " ++ (show d))

-- MAIN FUNCTION

main = do
  t_a
  t_b
  t_c
  t_d
