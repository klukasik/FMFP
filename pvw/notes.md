#### Random
- Equivalence Theorem
- Eq. Lemma 1
- Eq. Lemma 2
- Substition Lemma Operational Semantics

<!-- ##### -->

## Axiomatic Semantics
#### Contents
- Goal
- Hoare Logic
- Soundness & Completness

### Goal
- Partial correctness
- Total correctness

### Hoare Logic
#### Contents
- Hoare Triples and Assertions
- Derivation System
- Total Correctness (Termination)

#### Hoare Triples & Assertions
- Substitution Lemma still holds

#### Derivation System
- IMP derivation rules
- Semantic entailment
- Rule of consequence

#### Total Correctness
- Slightly different than Partial Correctness
-- if P true in sigma, then terminates and Q true in final state
-- Partial Correctness: if p true in sigma and statement terminates, then Q true in final state
- Total correctness derivation rule for loops


