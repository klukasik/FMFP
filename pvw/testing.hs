-- Test 1
test :: Int -> Int
test 0 = 1
test 0 = 0 -- gets ignored
test _ = 0


-- Test 2
{-
 - map as foldl
-}
map' f = foldl (\a b -> a ++ [f b]) []
{-
 - map as foldr
 -}
map'' f = foldr (\a b -> (f a) : b) []

-- Test 3
{-
 - t stands for a generic type (t is not a variable, its a type for the variable)
 -}
data Tree t = Leaf | Node t (Tree t) (Tree t)


