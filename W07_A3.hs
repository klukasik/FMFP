module Regexp where 

import Parsers

-- (a) define the type Regexp here
data Regexp = Lit Char | Seq Regexp Regexp | Or Regexp Regexp | Iter Regexp

instance Show Regexp where
  show (Lit c) = [c]
  show (Seq r1 r2) = show r1 ++ show r2
  show (Or r1 r2) = "(" ++ show r1 ++ "|" ++ show r2 ++ ")"
  show (Iter r) = show r ++ "+"

-- (b)

{-

Our strategy is to have parsers for each possible case.
Then we just want to apply all parsers to first char (unless its clearly not the one to apply).
Since we're running all parsers, one of them will be the correct one.
The correct parser will have some type of recursion (again we try all of it), 
which results in some parser being correct etc.

-}

pAtom :: Parser Regexp
pAtom = pChar ||| pParen
  where
    pChar = do
      c <- sat (`notElem` "()+|")
      return $ Lit c
    
    pParen = do
      char '('
      r <- pExpr
      char ')'
      return r

pIter :: Parser Regexp
pIter = pAtom ||| pPlus
  where
    pPlus = do
      r <- pAtom
      char '+'
      return $ Iter r

pSeq :: Parser Regexp
pSeq = pIter ||| pSeq'
  where
    pSeq' = do
      r1 <- pIter
      r2 <- pSeq
      return $ Seq r1 r2

pExpr :: Parser Regexp
pExpr = pSeq ||| pOr
  where
    pOr = do
      r1 <- pSeq
      char '|'
      r2 <- pExpr
      return $ Or r1 r2

str2regexp :: String -> Regexp
str2regexp = completeParse pExpr

-- (c)


regexpParser :: String -> Parser ()
regexpParser = undefined

matches :: String -> String -> Bool
matches = undefined
