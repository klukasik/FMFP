module SquareRoot where

-- small error margin
eps :: Double
eps = 0.001

-- first arg is number whose square root is searched
-- second arg is approximation
improve :: Double -> Double -> Double
improve x y = (y + (x / y)) / 2

-- first arg is prior approximation
-- second arg is current approximation
goodEnough :: Double -> Double -> Bool
goodEnough y1 y2
  | e < eps = True
  | otherwise = False

  where
    e = abs ( (y2 - y1) / y2 ) 

root :: Double -> Double
root x = y
  where
    f :: Double -> Double -> Double
    f z1 z2 
      | goodEnough z1 z2 	= z2  
      | otherwise 		= f z2 (improve  x z2)

    y = f 1 (improve x x)

main :: IO ()
main = do  
  putStrLn "Compute the root of:" 
  inp <- getLine  
  if (read inp :: Double) >= 0
  then do
    putStrLn ("Square root: " ++ (show (root (read inp))))
    main
    return ()
  else return ()

    

