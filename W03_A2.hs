module PrimeNumbers where

-- (a)
prime :: Int -> Bool
prime 1 = False
prime n = [x | x <- nat, (mod n x) == 0] == [1]
  where
    m = fromIntegral n
    r = floor (sqrt m)
    nat = [1 .. r]

-- (b)
primes :: Int -> [Int]
primes m = [p | p <- nat, prime p]
  where
    nat = [1 .. m]

-- (c)
firstPrimes :: Int -> [Int]
firstPrimes m = aux 2 m []
  where
    -- aux k m f
    -- k = current number
    -- m = nr of primes needed
    -- f will contain the primes
    -- aux iterates from 2 to mth-prime
    aux :: Int -> Int -> [Int] -> [Int]
    aux k m f 
      | length f == m   = reverse f
      | prime k         = aux (k+1) m (k : f)
      | otherwise       = aux (k+1) m f

