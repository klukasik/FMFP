module FileSystemEntries where

data FSEntry = Folder String [FSEntry] | File String String


{-
NOTE:
This task mainly focuses on FOLDING over DATA types
-}


-- (a)

fFSE :: (String -> [a] -> a) -> (String -> String -> a) -> FSEntry -> a
fFSE f g (File s1 s2) = g s1 s2
fFSE f g (Folder s xs) = f s (map (fFSE f g) xs)

-- (b)

number :: FSEntry -> Int
number = length . fFSE (\x y -> concat ([1]:y)) (\x y -> [1])

count :: Char -> FSEntry -> Int
count c = length . filter (\c' -> c' == c) . fFSE (\x y -> concat y) (\x y -> y)

-- (c)

paths :: FSEntry -> [String]
paths = fFSE (\x y -> [x ++ "/" ++ z | z <- concat y]) (\x y -> [x])

