module PropLogic where

import Data.List (union, subsequences)

-- define the data type Prop a here
data Prop a = Var a | Not (Prop a) | And (Prop a) (Prop a) | Or (Prop a) (Prop a)

foldProp 
  :: (a -> b)
  -> (b -> b)
  -> (b -> b -> b)
  -> (b -> b -> b)
  -> Prop a
  -> b

foldProp fv fn fc fd = aux
  where
    aux (Var x) = fv x
    aux (Not x) = fn (aux x)
    aux (And x y) = fc (aux x) (aux y)
    aux (Or x y) = fd (aux x) (aux y)

evalProp :: (a -> Bool) -> Prop a -> Bool
evalProp fv p = foldProp fv (not) (&&) (||) p

propVars :: Eq a => Prop a -> [a]
-- WORKS: but replace f -> UNION, from Data.List (union)
{-
propVars p = foldProp (\x -> [x]) id f f p
  where
    f :: Eq a => [a] -> [a] -> [a]
    f x y = foldl uniqJoin x y
      where
        uniqJoin :: Eq a => [a] -> a -> [a]
        uniqJoin l e
         | elem e l     = l
         | otherwise    = e : l
-}
propVars p = foldProp (\x -> [x]) id union union p

-- create all possible subsets of the variables obtained from propVars
-- use a subset as an indication whether a variable should be true or not,
-- i.e. if variable in list -> true, else not
-- loop through all possible subsets -> if one of them evals true -> satisfiable
satProp :: Eq a => Prop a -> Bool
satProp p = foldl orEval False subs
  where
    subs = subsequences (propVars p)
    orEval b ts = (||) b (evalProp (\x -> elem x ts) p)

instance Show a => Show (Prop a) where
  show (Var n)      = show n
  show (Not p)      = "(Not " ++ (show p) ++ ")"
  show (And p1 p2)  = "(" ++ (show p1) ++ " && " ++ (show p2) ++ ")"
  show (Or p1 p2)   = "(" ++ (show p1) ++ " || " ++ (show p2) ++ ")"
