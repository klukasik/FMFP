module Recursion where

-- Multiply two non-negative numbers. Only use addition/subtraction.
mult :: (Int, Int) -> Int
mult (x, y)
  | x == 0 	= 0
  | x < 0	= f (x * (-1)) (y * (-1)) (y * (-1))
  | otherwise 	= f x y y

  where
    f :: Int -> Int -> Int -> Int
    f i z y1
      | i == 0 		= 0
      | i == 1 		= z
      | otherwise 	= f (i - 1) (z + y1) y1

-- Integer part of the base 2 logarithm.
log2 :: Int -> Int
log2 x = let
  f :: Int -> Int -> Int
  f a i
    | (a * 2) > x 	= i
    | otherwise		= f (a * 2) (i + 1)

  in f 1 0

-- Test whether the integer is a prime number.
-- We can iterate through all numbers that are smaller equal root, check whether it divides the number
-- we can skip even numbers, if it isn't dividable by two
isPrime :: Int -> Bool
isPrime x
  | x <= 1		= False
  | x <= 3		= True
  | (mod x 2) == 0	= False
  | otherwise		= f 3
  
  where
    f :: Int -> Bool
    -- f d returns whether y (the absolute value of x) is dividable by any odd number bigger equal d and smaller euqal the root of y
    f d
      | (mod x d) == 0					= False
      | (d + 2) <= floor (sqrt (fromIntegral x))	= f (d + 2)
      | otherwise					= True

