module OneTimePad where

-- map f [a1, a2, ...]
-- gives [f a1, f a2, ...]

-- zip [a1, a2, ...] [b1, b2, ...]
-- gives [(a1, b1), (a2, b2), ...]

xor :: Bool -> Bool -> Bool
xor a b = (a || b) && not (a && b)

-- a)

otp :: [Bool] -> [Bool] -> [Bool]
otp m k = map (uncurry xor) (zip m k)

-- b)

opt2 :: [Bool] -> [Bool] -> [Bool]
opt2 m k = zipWith xor m k


