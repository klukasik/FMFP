module DFA where

import Prelude hiding (Word)

type State = Int
type Alphabet a = [a]
type DFA a = 
  ( Alphabet a             -- alphabet
  , State                  -- initial state
  , State -> a -> State    -- transition function
  , State -> Bool)         -- test for final state
type Word a = [a]

alphabet :: DFA a -> Alphabet a
alphabet (sigma, _, _, _) = sigma

initial :: DFA a -> State
initial (_, q0, _, _) = q0

transition :: DFA a -> (State -> a -> State)
transition (_, _, delta, _) = delta

finalState :: DFA a -> State -> Bool
finalState (_, _, _, final) = final

{-
   Please indicate briefly why using accessor functions is useful.
   
   ...
-}

accepts :: DFA a -> Word a -> Bool
accepts d w = finalState d (foldl t q0 w)
  where 
    t = transition d
    q0 = initial d

-- foldl -> we iterate through [1 .. l]
-- for each iteration i,
-- we have a set of words ws of length i-1,
-- ws' = [c : w | w <- ws, c <- al]
-- we pass ws' to next iteration, i.e. this is our result

f :: Alphabet a -> [Word a] -> Int -> [Word a]
f al ws _ = [c : w | c <- al, w <- ws]

makeWords :: Alphabet a -> [Word a]
makeWords al = map (\c -> [c]) al

lexicon :: Alphabet a -> Int -> [Word a]
lexicon al 0 = [[]]
lexicon al l = foldl (f al) (makeWords al) [1 .. (l-1)]

language :: DFA a -> Int -> [Word a]
language d l = [w | w <- ws, accepts d w]
  where ws = lexicon (alphabet d) l

-- Try to use map, foldl, foldr, filter and/or list comprehensions.
