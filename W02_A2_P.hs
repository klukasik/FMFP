module Coins where

cntChange :: Int -> Int
cntChange x = changeL x coins

coins = [500, 200, 100, 50, 20, 10, 5]


-- If input b true, then can't check whether value could be a single coin
--change :: Int -> Int
--change x = changeL x coins

changeL :: Int -> [Int] -> Int

changeL 0 _ = 1
changeL _ [] = 0

changeL x (c:cs)
  | (x - c) < 0 	= changeL x cs
  | (x - c) > 0     	= (changeL (x-c) (c:cs)) + (changeL x cs)
  | (x - c) == 0    	= 1 + (changeL x cs)

