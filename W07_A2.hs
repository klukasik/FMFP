module Polynomials where

data Monomial a = Mono Integer [a]
 deriving (Eq, Ord, Show)
type Poly a = [Monomial a]

data SymbExpr a
  = Var a
  | Lit Integer 
  | Add (SymbExpr a) (SymbExpr a)
  | Mul (SymbExpr a) (SymbExpr a)
  deriving Show

-- (a)
-- we fold over [Monomial a]
-- each element holds an integer and [a], i.e. list of a-elements
-- Task says, [a] is the variable name, and Integer the associated value
evalPoly :: (a -> Integer) -> Poly a -> Integer
evalPoly f = foldr (\(Mono v xs) y -> v * (mult (map f xs)) + y) 0
  where
    mult :: Num a => [a] -> a
    mult = foldr (*) 1 

-- (b)

foldSymbExpr :: (a -> b) -> (Integer -> b) -> (b -> b -> b) -> (b -> b -> b) -> SymbExpr a -> b
foldSymbExpr f1 f2 f3 f4 = g
  where
    g (Var x) = f1 x
    g (Lit v) = f2 v
    g (Add se1 se2) = f3 (g se1) (g se2)
    g (Mul se1 se2) = f4 (g se1) (g se2)

{- (c)



-}

-- (d)

toPoly :: SymbExpr a -> Poly a
toPoly = foldSymbExpr f1 f2 f3 f4
  where
    f1 :: a -> Poly a
    f1 x = [Mono 1 [x]]

    f2 :: Integer -> Poly a
    f2 x = [Mono x []]

    f3 :: Poly a -> Poly a -> Poly a
    f3 = (++)

    f4 :: Poly a -> Poly a -> Poly a
    f4 p1 p2 = [(Mono (v1*v2) (l1++l2)) | (Mono v1 l1) <- p1, (Mono v2 l2) <- p2]
