module Misc where

-- (a)

match :: String -> Bool
match = g . foldr f []
  where
    g :: [Int] -> Bool
    g xs = length xs == 0

    f :: Char -> [Int] -> [Int]
    f ')' xs = push 0 xs
    f '}' xs = push 1 xs
    f '(' xs = pop 0 xs
    f '{' xs = pop 1 xs
    f _ xs   = xs

    push :: Int -> [Int] -> [Int]
    push x xs = x:xs

    pop :: Int -> [Int] -> [Int]
    pop _ [] = [2]
    pop 0 (0:xs) = xs
    pop 1 (1:xs) = xs
    pop _ _ = [2]


-- (b)

risers :: Ord a => [a] -> [[a]]
risers = reverse . map g . foldl f [] 
  where
    -- for lower runtime maybe
    g :: Ord a => [a] -> [a]
    g xs = reverse xs

    f :: Ord a => [[a]] -> a -> [[a]]
    f [] y = [[y]]
    f (x:xs) y
      | head x <= y     = (y:x):xs
      | otherwise       = [y] : (x:xs)
     
risers' :: Ord a => [a] -> [[a]]
risers' [] = []
risers' xs = foldl (\ys y -> if (last $ last ys) <= y then (init ys) ++ [last ys ++ [y]] else ys ++ [[y]]) [[head xs]] (tail xs)
