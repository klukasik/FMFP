module Palindromes where

{-
## Notes

A palindrome of even length 
- => s[0, len/2 -1] == reverse s[len/2 + 1, len-1]

A palindrome of uneven length
- => s[0, floor (len/2)] == reverse s[ceiling (len/2), len-1]

A potential palindrome is a concatenation of two elements of list l

-}

palindromes :: [String] -> [String]
palindromes l = [s | 
  v <- l, w <- l, let s = v ++ w,
  take (floor (fromIntegral (length s) / 2)) s == take (floor (fromIntegral (length s) / 2)) (reverse s)
  ]
