module Complex where

-- Write your implementation of complex numbers here.

re :: (Double, Double) -> Double
re (a, b) = a

im :: (Double, Double) -> Double
im (a, b) = b

conj :: (Double, Double) -> (Double, Double)
conj (a, b) = (a, ((-1) * b))

add :: (Double, Double) -> (Double, Double) -> (Double, Double)
add (a, b) (c, d) = ((a + c), (b + d))

mult :: (Double, Double) -> (Double, Double) -> (Double, Double)
mult (a, b) (c, d) = let
  x1 = (a * c) + ((-1) * (b * d))
  x2 = (a * d) + (c * b)
  in (x1, x2)

absv :: (Double, Double) ->  Double
absv (a, b) = sqrt ((a * a) + (b * b))

-- MAIN FUNCTION

main :: IO ()
main = do
  putStrLn "Enter your complex number's real component: "
  inp1 <- getLine
  putStrLn "Enter your complex number's imaginary component: "
  inp2 <- getLine
  
  let
    a = read inp1
    b = read inp2
    x = absv (a, b)
  putStrLn ("Your complex number's absolute value is: " ++ (show x) )
