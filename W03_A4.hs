module Words where

-- returns first String splitted by char c,
-- and the rest of the String,
-- removing the one splitting character
takeSplit :: Char -> String -> (String, String)
takeSplit c s = aux s ""
  where
    -- aux k r
    -- k = string still to split
    -- r = reversed String that's being extracted
    aux :: String -> String -> (String, String)
    aux "" r = (reverse r, "")
    aux (k:ks) r
      | k == c      = (reverse r, ks)
      | otherwise   = aux ks (k : r)


-- (a)
split :: Char -> String -> [String]
split c "" = [""]
split c s 
  | head (reverse s) == c   = (aux s []) ++ [""]
  | otherwise               = aux s []
    where
      -- aux k r
      -- k = string still to split
      -- r = reversed list of splitted strings
      aux :: String -> [String] -> [String]
      aux "" r    = reverse r
      aux k r     = aux (snd tup) (fst tup : r)
        where
          tup = takeSplit c k


-- (b)
isSpace :: Char -> Bool
isSpace c = (c == ' ')

toWords :: String -> [String]
toWords "" = []
toWords s = [w | w <- (split ' ' s), w /= ""]

-- (c)
countWords :: String -> Int
countWords s = length (toWords s)
