module NQueens where

-- Implement a function that generates all possible board assignments.

generate :: Int -> [[Int]]
-- we first create a list for every queen
-- also, it alredy contains the position of the n-th queen
generate n = aux n [[x] | x <- [1 .. n]]
  where
    -- aux iterates through every list
    -- in the (n-i+1)-th iteration, we have all possibilities for (n-i+1) queens on an n*n chessboard
    aux :: Int -> [[Int]] -> [[Int]]
    aux 1 ys = ys
    aux i ys = aux (i-1) (concat (map (special n) ys))

-- spcial takes a list, and creates n copies which have 0, 1 .. n prepended to them
special :: Int -> [Int] -> [[Int]]
special n xs = init $ foldl f [(0:xs)] (take n (repeat [(0:xs)]))
  where
    f :: [[Int]] -> [[Int]] -> [[Int]]
    f ((x:xs):ys) _ = ((x + 1) : xs) : (x:xs) : ys 

-- Implement a function that tests whether a given assignment is valid.

test :: [Int] -> Bool
test xs = ([1..length xs] == qsort xs) && diag xs 1

diag :: [Int] -> Int -> Bool
diag [] _ = True
diag (x:xs) i = c1 && c2 && diag xs (i+1) 
  where
    n = i + length xs 
    c1 = [(k1, k2) | (k1, k2) <- zip [x+1..n] xs, k1 == k2] == []
    c2 = [(k1, k2) | (k1, k2) <- zip [x-1, x-2 .. 1] xs, k1 == k2] == []

qsort [] = [] 
qsort (p:xs) = (qsort [x | x<-xs, x < p]) ++ [p] ++ (qsort [x | x<-xs, x >= p])

naivequeens :: Int -> [[Int]]
naivequeens n = filter test $ generate n


{-
  Headache of the week:
  Implement a function that solves this n queens problem in a more efficient way
  such that partial assignments get tested (whether they may be a valid full
  assignment) as early as possible. 
-}

queens :: Int -> [[Int]]
queens n = [xs | (xs, b) <- result]
  where
    b0 = [(x, y) | x <- [1..n], y <- [1..n]]
    remo0 y = remo n b0 (n, y)
    r0 = [([y], remo0 y) | y <- [1 .. n]]

    result = aux n r0

    aux :: Int -> [([Int], [(Int, Int)])] -> [([Int], [(Int, Int)])]
    aux 1 r = r
    aux i r = aux (i-1) (concat $ map (special2 n) r)


remo :: Int -> [(Int, Int)] -> (Int, Int) -> [(Int, Int)]
-- (x, y) is new queen, b is free spaces on board
remo n b (x, y) = filter f $ filter g b
  where
    f = \(x', y') -> (x' /= x) && (y' /= y)
    g = \(x', y') -> not $ elem (x',y') [t | t <- concat [l1, l2]]
      where
--        l1 = concat [(zip [x+1 .. n] [y+1 .. n]), (zip [x-1, x-2 .. 1] [y+1 ..n])]
--        l2 = concat [(zip [x+1 .. n] [y-1, y-2 .. 1]), (zip [x-1, x-2 .. 1] [y-1, y-2 .. 1])]
--        l1 = (zip [x+1 .. n] [y+1 .. n])
--        l2 = (zip [x+1 .. n] [y-1, y-2 .. n])
        l1 = zip [x-1, x-2 ..1] [y+1 .. n]
        l2 = zip [x-1, x-2 ..1] [y-1, y-2 .. 0]

-- possible ys at x-pos x
possy x b = [y' | (x', y') <- b, x' == x]

special2 :: Int -> ([Int], [(Int, Int)]) -> [([Int], [(Int, Int)])]
special2 n (xs, b) = foldl f [] ys
  where
    x = n - length xs
    ys = possy x b

    f :: [([Int], [(Int, Int)])] -> Int -> [([Int], [(Int, Int)])]
    f r y = ((y:xs), remo n b (x, y)) : r

