module Redefine where

{-
   In this exercise you are required to adapt the following function
   definitions of f, g and h such that foldl, foldr, zip, zipWith, filter,
   curry, uncurry, etc. will be used. That means, your task is to modify the
   lines 11-12, 16-20 and 24-29.
-}

f :: [[a]] -> [a]
f [] = []
-- f (x:xs) = reverse x ++ f xs
f l = foldr (\a b -> (reverse a) ++ b) [] l



g :: Eq a => [a] -> [a] -> [a]
g [] _ = []
g _ [] = []
{-
g (x:xs) (y:ys)
        | x == y = x : g xs ys
        | otherwise = g xs ys
-}

g x y = map (\(a, b) -> a) (filter (\(a, b) -> a==b) (zip x y))


h :: [Int] -> Int
{-
h = aux 0
   where
      aux z [] = z
      aux z (x:xs)
        | even x = aux (1 + z) xs
        | otherwise = aux z xs
-}
h x = length (filter even x)
        
