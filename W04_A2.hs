module ConcatMap where

import Prelude hiding (sum)

concatMap' :: (a -> [b]) -> [a] ->[b]
concatMap' f = foldr aux e
  where 
    -- aux receives element of [a], and a list [b]
    -- aux transforms a -> [b] 
    -- and concatinates with [b]
    aux x r = (f x) ++ r

    e   = []


