
module Lists where

-- We use different names to prevent clashes with the Prelude.

map' :: (a -> b) -> [a] -> [b]
map' f xs = [f x | x <- xs]

filter' :: (a -> Bool) -> [a] -> [a]
filter' f xs = [x | x <- xs, f x]

concatMap' :: (a -> [b]) -> [a] -> [b]
concatMap' f xs = [y | x <- xs, y <- f x]
