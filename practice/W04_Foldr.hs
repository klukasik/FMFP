module Foldr where

and' xs = foldr (&&) True xs
or' xs = foldr (||) False xs
sum' xs = foldr (+) 0 xs
product' xs = foldr (*) 1 xs
concat' xs = foldr (++) [] xs

all' f = foldr (\x y -> (f x) && y) True
any' f = foldr (\x y -> (f x) || y) False


