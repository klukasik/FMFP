-- Haskell Data Types
-- https://mmhaskell.com/liftoff/data-types
-- Book ProgrammingHaskell

{-
When definiting a data type we give it "Constructors".
We can treat constructors as kind of an "enum" type, or more precise an enum "value",
however, constructors can have arguments.

Constructors differ from normal functions, 
since they have no defining equation,
they are rather given as "arguments" to a function,
or more precise as the specific type/constructor of a data type.
-}

-- Ex. 1
data Parent = Mother | Father

f :: Parent -> String
f Mother = "Mama"
f Father = "Dada"

-- Ex. 2
data Feels = Grumpy | Great
data State = Old Feels | Young Feels

g :: State -> String
g (Old Grumpy) = "I'll die soon"
g (Young Great) = "Let's go skydiving"
g (Young Grumpy) = "I need a coffee"
g (Old Great) = "Here's some cash for candy"

-- Ex. 3, deriving Equality
data Child = Son | Daughter
  deriving (Eq)

h :: Child -> String
h c
  | c == Son 		= "It's a boy"
  | c == Daughter	= "It's a girl"

-- Ex. 4, Constructors with arguments
data Runner = Active Integer | Retired Integer Integer

g :: Runner -> String
g (Active t) = "Active, runs 1 km in " ++ (show t) ++ " seconds"
g (Retired age trophies) = "Retired, age: " ++ (show age) ++ ", trophies: " ++ (show trophies)
