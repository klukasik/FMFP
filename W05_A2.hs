module Tree where

import Data.List (elemIndex, elemIndices)

data Tree t = Leaf | Node t (Tree t) (Tree t)

-- interval
type Invl a = (a, a)


-- app takes a Tuple (children, nodes) 
-- and a Tree T,
-- and returns a Tuple with the children of T appended,
-- and with the nodes value appended to nodes
app :: ([Tree t], [t]) -> Tree t -> ([Tree t], [t])
app x Leaf = x
app (cs, vs) (Node v c1 c2) = (cs ++ [c1] ++ [c2], vs ++ [v])

-- traverses nodes, returns children
-- and the node values appended to second param
travers :: [Tree t] -> [t] -> ([Tree t], [t])
travers ts vs = foldl app ([], vs) ts
    
bfs :: Tree t -> [t]
bfs t = aux [t] []
  where
    aux :: [Tree t] -> [t] -> [t]
    aux [] res = res
    aux lev res = aux (fst z) (snd z)
      where
        z = travers lev res


-- b)

sorted :: Ord t => [t] -> Bool
sorted (x:y:xs) = x < y && (sorted (y:xs))
sorted _ = True

inOrder :: Tree t -> [t]
inOrder Leaf = []
inOrder (Node x l r) = res1 ++ [x] ++ res2
  where
    res1 = inOrder l
    res2 = inOrder r







sortedTree :: Ord t => Tree t -> Bool
sortedTree = sorted . inOrder
