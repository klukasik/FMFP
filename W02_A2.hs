module Coins where

cntChange :: Int -> Int
cntChange x = changeL x coins

coins = [5, 10, 20, 50, 100, 200, 500]


-- If input b true, then can't check whether value could be a single coin
--change :: Int -> Int
--change x = changeL x coins

changeL :: Int -> [Int] -> Int

changeL 0 _ = 1
changeL _ [] = 0

changeL x (c:cs)
  | (x - c) > 0 	= (changeL (x-c) (c:cs))  + (changeL x cs)
  | (x - c) == 0  	= 1
  | otherwise 		= 0

